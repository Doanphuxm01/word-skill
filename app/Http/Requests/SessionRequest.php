<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SessionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|min:3',
            'description' => 'required',
            'speaker' => 'required',
            'type' => 'required',
            'description' => 'required',
            'start' => 'required',
            'end' => 'required',
            'end' => 'after:start'
        ];
    }
    public function messages()
    {

        return [
            'start.required' => 'Please, fill in the initial time',
            'end.required' => 'Please, fill in the end time.',
            'end.after' => 'The end time must be greater than initial time.'
        ];

    }
}
