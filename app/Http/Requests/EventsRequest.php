<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EventsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
                'name' => 'required|min:3',
                'slug' => 'required|unique:events,slug,slug|max:191',
                'date'  => 'date_format:Y-m-d'
        ];
    }

    public function messages(){
        return [
            'required' => ':attribute may not be blank',
            'name.required' => ':attribute may not blank',
            'slug.required' => ':attribute may not blank',
            'unique'   => ':attribute not duplicate',
            'slug'     => ':attribute Slug must not be empty and only contain a-z,0-9 and'
        ];

    }

    public function attributes(){
        return[
            'name' => 'name',
            'slug' => 'slug',
            'date' => 'date',
        ];
    }
}
