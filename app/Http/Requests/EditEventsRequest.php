<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditEventsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:3',
            'slug' => 'required',
            'date'  => 'date_format:Y-m-d'
        ];
    }
    public function messages(){
        return [
            'required' => ':attribute may not be blank',
            'name.required' => ':attribute may not blank',
            'slug.required' => ':attribute may not blank',
            'slug'     => ':attribute Slug must not be empty and only contain a-z,0-9 and'
        ];

    }
    public function attributes(){
        return[
            'name' => 'name',
            'date' => 'date',
        ];
    }
}
