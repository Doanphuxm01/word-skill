<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Channels_Models;
use App\Models\EventsModels;

class ChannelsController extends Controller
{
    protected $events;

    protected $channels;

    public function __construct(EventsModels $event,Channels_Models $channels){
        $this->events = new EventsModels();
        $this->channels = new Channels_Models();
    }

    public function create(Request $request)
    {
        if($request->isMethod('post')){
            $id = $request->id;
            $events = $this->events->getDetailEvents($id);
            // $save = $request->all();
            $article = Channels_Models::create([
                'event_id' => $request->id,
                'name' => $request->name,
            ]);
            // return  redirect()->route('events.detail');
            return redirect()->route('events.detail', ['id' => $id]);
        }
        $id = $request->id;
        $events = $this->events->getDetailEvents($id);
        // dd($events);
        $view = view('admin.dashboard.channels.create');
        $view->with('events',$events);
        return $view;
    }

    public function save(Request $request){
        $id = $request->id;
        dd($id);
    }

}
