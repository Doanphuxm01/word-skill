<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Channels_Models;
use App\Models\Room_Models;
use App\Models\EventsModels;
class RoomsController extends Controller
{
    protected $rooms;
    protected $events;
    protected $channels;
    public function __construct(EventsModels $event,Room_Models $rooms,Channels_Models $channels){
        $this->rooms = new Room_Models();
        $this->events = new EventsModels();
        $this->channels = new Channels_Models();
    }
    public function index(Request $request)
    {
        dump(session()->get('event_id'));
        if($request->isMethod('POST')){
            $id = $request->id;
            $events = $this->events->getDetailEvents($id);
            $article = new Room_Models;
            $article->name       = $request->name;
            $article->channel_id = $request->channel_id;
            $article->capacity   = $request->capacity;
            $article->save();
            return redirect()->route('events.detail', ['id' => $id]);
        }
        $id = $request->id;
        $channels = $this->channels->getEventByChannelsId($id);
        $events = $this->events->getDetailEvents($id);
        $view = view('admin.dashboard.rooms.create');
        $view->with('channels',$channels);
        $view->with('events',$events);
        return $view;
    }

     public function sendData( Request $request ){
//         dd($request->all());
         $id = session()->get('event_id');
//         dd($id);
         $events = $this->events->getDetailEvents($id);
         $article = Room_Models::create([
             'name'        => $request->name,
             'channel_id' => $request->channel_id,
             'capacity'   => $request->capacity,
         ]);
         return redirect()->route('events.detail', ['id' => $id]);
     }
}
