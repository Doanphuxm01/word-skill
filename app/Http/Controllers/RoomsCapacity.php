<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Room_Models;
use DB;
class RoomsCapacity extends Controller
{
    public function __construct(Room_Models $rooms){
        $this->rooms = new Room_Models();
    }

    public function index(Request $request)
    {
        $get_Data = Room_Models::all();
        $view = view('admin.dashboard.room_caption.index');
        return $view;
    }
    public function charJS(Request $request)
    {
        $result = Room_Models::select('name','capacity')
                                ->orderby('capacity', 'ASC')
                                ->get();
        return response()->json($result);
    }
}
