<?php

namespace App\Http\Controllers\Auth;
// use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;
use App\User;
use Illuminate\Support\MessageBag;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = 'home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function organizers(){
        return 'organizers';
    }

    public function login(Request $request){
            if($request->isMethod('POST')){
                $all = $request->all();
                $data = [
                    'email' => $all['email'],
                    'password_hash' => $all['password_hash'],
                ];
                try {
                    $user = User::where($data)->first();
                    if($user){
                        Auth::login($user);
                        return redirect()->route('events.index');
                    }else{
                        $errors = new MessageBag(['errorlogin' => 'Email hoặc mật khẩu không đúng']);
                        return redirect()->back()->withInput()->withErrors($errors);
                }
                } catch (ModelNotFoundException $e){
                    return redirect()->route('admin.login');
                }
            }else{
                return redirect()->route('admin.login');
            }
    }

    public function logout(){
        Auth::logout();
        return redirect()->route('admin.login');
    }
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
}
