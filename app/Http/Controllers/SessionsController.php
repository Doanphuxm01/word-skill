<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\EventsModels;
use App\Models\Room_Models;
use App\Models\Session_Models;
use App\Http\Requests\SessionRequest;
class SessionsController extends Controller
{
    protected $events;
    public function __construct(EventsModels $event,Room_Models $rooms){
        $this->events = new EventsModels();
        // $this->tickets = new event_ticketsModels();
        // $this->channels = new Channels_Models();
        $this->rooms = new Room_Models();
    }
     public function create(Request $request)
    {
        $id = $request->id;
        $events = $this->events->getDetailEvents($id);
        $rooms = Room_Models::all();
        $view = view('admin.dashboard.sessions.create');
        $view ->with('events',$events);
        $view ->with('rooms',$rooms);
        return $view;
    }

    public function add(SessionRequest $request){
        $id = $request->id;
        $events = $this->events->getDetailEvents($id);
        $article = Session_Models::create([
            'type' => $request->type,
            'title' => $request->title,
            'room_id' => $request->room_id,
            'speaker' => $request->speaker,
            'cost' => $request->cost,
            'start' => $request->start,
            'end' => $request->end,
            'description' => $request->description,
        ]);
        return redirect()->route('events.detail', ['id' => $id]);
    }
}
