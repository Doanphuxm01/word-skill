<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\EventsModels;
use Illuminate\Support\Facades\Response;
use App\User;

class EventsController extends Controller
{
    protected $events;
    protected $user;
    public function __construct(EventsModels $event, User $user){
        $this->events = new EventsModels();
        $this->user = new User();
    }

    public function getDataSlug(Request $request, $organzer = '', $event = ''){
        $data = [
            'origanzers' => $organzer,
            'event' => $event,
        ];
        $organzer_id = $data['origanzers'];
//        dd($organzer_id);
        if($organzer_id == ''){
            return response()->json($data,array(
                'code' => 401,
            ));
        }
        $event_id    = $data['event'];
        $data = $this->events->getEventByOrganizerId(isset($organzer_id) ? $organzer_id : ' ');

        return response()->json($data, 200);
    }

    public function index(Request $request){
        $events = $this->events->allEvents();
        if(isset($events) && $events == null){
            return Response::json('Có lỗi xảy ra.', 500);
        }
        return response()->json(['events' => $events],200);
    }
}
