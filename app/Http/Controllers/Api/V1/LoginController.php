<?php

namespace App\Http\Controllers\APi\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use App\User;
use Illuminate\Support\MessageBag;
class LoginController extends Controller
{
    use AuthenticatesUsers;

    public function login(Request $request){
        if($request->isMethod('POST')){
            $all = $request->all();
            $data = [
                'email' => $all['email'],
                'password_hash' => $all['password_hash'],
            ];
            try {
                $user = User::where($data)->first();
                if($user){
                    Auth::login($user);
                    return response()->json('Login thanh cong',200);
                }else{
                    $errors = new MessageBag(['errorlogin' => 'email hoac  mat khau khong dung','errors' => '401']);
                    return response()->json($errors,401);
                }
            } catch (ModelNotFoundException $e){
                return response()->json('dang nhap that bai',401);
            }
        }else{
            return response()->json('dang nhap that bai',401);
        }
    }

    public function logout(){   
        Auth::logout();
        return response()->json('logout thanh cong',200);
    }
}
