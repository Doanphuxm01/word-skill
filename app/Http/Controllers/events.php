<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\EventsRequest;
use App\Http\Requests\EditEventsRequest;
use DB;
use App\Models\EventsModels;
use App\Models\event_ticketsModels;
use App\Models\Channels_Models;
use App\Models\Room_Models;
use App\Models\Session_Models;
class events extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    protected $events;
    protected $tickets;
    protected $channels;
    protected $rooms;
    protected $session;

    public function __construct(EventsModels $event,event_ticketsModels $tickets,Channels_Models $channels,Room_Models $rooms,Session_Models $session){
        $this->events = new EventsModels();
        $this->tickets = new event_ticketsModels();
        $this->channels = new Channels_Models();
        $this->rooms = new Room_Models();
        $this->session = new Room_Models();
    }

    public function index()
    {
        $events = $this->events->getEventByOrganizerId(\Auth()->user()->id);
        $view = view('admin.dashboard.events.index');
        $view ->with('events',$events);
        return $view;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $view = view('admin.dashboard.events.create');
        return $view;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EventsRequest $request)
    {
            $all = $request->all();
            EventsModels::create($all);
            session()->flash('success','create events success');
            return  redirect()->route('events.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,$id)
    {
        // $id = $request->id;
        $events = $this->events->getDetailEvents($id);
        $events = EventsModels::find($id);
        // dd($events);
        $view = view('admin.dashboard.events.edit');
        $view ->with('events', $events);
        return $view;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditEventsRequest $request, $id)
    {
        $events = $request->all();
        EventsModels::find($id)->update($events);
        session()->flash('success','edit events success');
        return redirect()->route('events.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function detail( Request $request )
    {
            $results = [];
            $id = $request->id;
            $event_rooms = EventsModels::with('channels','channels.rooms')->where('id',$id)->first();
            foreach ($event_rooms->channels as $key => $channels){
                foreach ($channels->rooms as $k => $rooms){
                    $results[] = [
                        'id'       => $rooms['id'],
                        'name'     => $rooms['name'],
                        'capacity' => $rooms['capacity']
                    ];
                }
            }
            session()->put('event_id', $id);
            $events_id = dump(session()->get('event_id'));
            $events = $this->events->getDetailEvents($id);
            $tickets = $this->tickets->showData($id);
            $channels = $this->channels->getEventByChannelsId($id);
            $session = Session_Models::all();
            // $session = Session_Models::with('rooms','rooms.channels.events')->where('id',$id)->first();
            // $session = $this->session->getDataByIdEvent($id);
            // dd($session);
            $session =Session_Models::paginate(5);
            $view = view('admin.dashboard.events.detail');
            $view ->with('events',$events);
            $view ->with('tickets',$tickets);
            $view ->with('channels',$channels);
            $view ->with('rooms',$results);
            $view ->with('session',$session);
            return $view;
    }
}
