<?php

namespace App;

use App\Models\EventsModels;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected  $table = 'organizers';

//    protected $fillable = [
//        'name', 'email', 'password',
//    ];

    protected $fillable = [
        'name','slug ', 'email', 'password_hash',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password_hash', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public function Event(){
        return $this->hasMany(EventsModels::class);
    }

    public function getSlug(){
        return $this->select('id','slug')->get();
    }
}
