<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Session_Models extends Model
{
    public $timestamps = false;
    protected $table = 'sessions';
    protected $guarded = [];

    public function getDataByIdEvent($id){
        return $this->where('room_id',$id)
                    ->select('id','room_id','title','description','speaker','type','cost','start','end')
                    ->with('channels','channels.events')
                    ->get();
    }

    public function rooms(){
        return $this->belongsTo(Room_Models::Class,'room_id','id');
    }
}
