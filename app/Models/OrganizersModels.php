<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrganizersModels extends Model
{
    protected $table = 'organizers';
    protected $fillable = [
        'name',
        'slug',
        'email',
        'password_hash'
    ];
    protected $guarded = [];

    public function Event(){
        return $this->hasMany(EventsModels::class,'id ','id');
    }
}
