<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EventsModels extends Model
{
    public $timestamps = false;
    protected $table = 'events';
    // protected $fillable = [
    //     'organizer_id',
    //     'name',
    //     'slug',
    //     'date'
    // ];
    protected $guarded = [];

    public function tickest()
    {
        return $this->hasMany(event_ticketsModels::class,'id','id');
    }

    public function organizers()
    {
        return $this->beLongsTo(OrganizersModels::class,'id','id');
    }

    public function channels()
    {
        return $this->hasMany(Channels_Models::class,'event_id','id');
    }

    public function allEvents(){
        return $this->select('id','organizer_id','slug','name','date')
                    ->with('organizers')
                    ->get();
    }

    public function getEventByOrganizerId($id) {
        return $this->where('organizer_id', $id)->select('id','organizer_id','slug','name','date')
                    ->with('organizers')
                    ->get();
    }
    public function getDetailEvents($id){
        return $this->where('id',$id)->get();
    }

    public static function checkEventByIdOrganizer($id_admin, $id) {
        $model = self::find($id);
        if(!empty($model) && (int)$id_admin == $model['organizer_id']) {
            return true;
        }
        return false;

    }
    public static function getDetailEventById($id) {
        return self::with(['tickets', 'channels', 'channels.rooms', 'channels.rooms.sessions'])->where('id', $id)->first()->toArray();
    }

    public function getDataEvenForOzganzer($id , $id_events){
        $data = $this->where('event_id',$id_events)->select('event_id')->with();
    }
}
