<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class event_ticketsModels extends Model
{
    public $timestamps = false;
    protected $table = 'event_tickets';
    protected $fillable = [
        'event_id',
        'name',
        'cost',
        'special_validity'
    ];
    protected $guarded = [];

//    public function Events()
//    {
//        return $this->belongsTo(EventsModels::class);
//    }
        public function Events()
    {
        return $this->belongsTo(EventsModels::class,'event_id','id');
    }

    public function showData($id){
        return $this->where('event_id',$id)
                    ->select('event_id','name','cost','special_validity')
                    ->with('events')
                    ->get();
    }
}
