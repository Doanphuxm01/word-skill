<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Channels_Models extends Model
{
    //
    public $timestamps = false;
    protected $table = 'channels';
    protected $guarded = [];

    public function events(){
        return $this->beLongsTo(EventsModels::class,'event_id','id');
    }

    public function rooms(){
        return $this->hasMany(Room_Models::class,'channel_id','id');
    }
    public function getEventByChannelsId($id) {
        return $this->where('event_id', $id)->select('event_id','id','name')
                    ->with('events')
                    ->get();
    }
}
