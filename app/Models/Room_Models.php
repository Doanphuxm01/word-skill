<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Room_Models extends Model
{
    public $timestamps = false;
    protected $table = 'rooms';
    protected $guarded = [];

    public function channels(){
        return $this->beLongsTo(Channels_Models::class,'channel_id','id');
    }
    public function session(){
        return $this->beLongsTo(Session_Models::class,'id','id');
    }

    // public function channelsGetEvent($id){
    //     return $this->where()
    // }
    public function getRoomsById($id) {
        return $this->where('channel_id', $id)->select('id','channel_id ','capacity')
                    ->with('channels')
                    ->get();
    }
    public function getRoomsByIdEvents($id) {
        return $this->where('channel_id', $id)->select('id','channel_id','capacity','name')
                    ->with('channels','channels.events')
                    ->get();
    }

}
