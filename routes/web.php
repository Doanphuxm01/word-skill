<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//use Illuminate\Routing\Route;

Route::get('/', function () {
    return view('welcome');
})->name('admin.login');
Auth::routes();
Route::group([
    'namespace' => 'Auth'
], function(){
    Route::match(['get', 'post'], 'login', 'LoginController@login')->name('admin.post.login');

    Route::match(['get', 'post'], 'logout', 'LoginController@logout')->name('admin.logout');

    Route::post('register', 'RegisterController@create')->name('admin.register');

});

Route::group(['prefix' => 'home'], function () {
    Route::get('', 'HomeController@index')->name('home');
    Route::any('{id}/events-detail', 'events@detail')->name('events.detail');
    Route::resource('events', 'events');
    Route::resource('reports', 'reports');
    Route::resource('tickets', 'tickets');

    Route::match(['get', 'post'], '{id}/channels','ChannelsController@create')->name('channels.create');
    Route::match(['get', 'post'], '{id}/sessions','SessionsController@create')->name('sessions.create');
    Route::match(['get', 'post'], '{id}/add','SessionsController@add')->name('sessions.add');
    Route::match(['get', 'post'], '{id}/rooms','RoomsController@index')->name('rooms.index');
    Route::match(['get', 'post'], 'create','RoomsController@sendData')->name('rooms.create');
    Route::any('math','RoomsCapacity@index')->name('admin.rooms');
    Route::any('charjs','RoomsCapacity@charJS')->name('admin.charJs');
});

