<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::group(['namespace' => 'Api\v1'],function(){
    Route::post('login','LoginController@Login');
    Route::post('logout','LoginController@logout');
});
Route::group([
    'namespace' => 'Api\V1',
 ],function (){
    Route::group(['prefix' => 'events'],function (){
        Route::get('','EventsController@index');
    });
    // Route::get('organizers/{organizer-slug}/events/{event-slug}','EventsController@getDataSlug');
    // Route::get('organizers/{organizer-slug}','EventsController@getDataSlug');
    // Route::get('organizers','EventsController@getDataSlug');
    Route::get('organizers/{organzer?}/events/{event?}', 'EventsController@getDataSlug');
});
