@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <div class="row">
                <main class="col-md-6 mx-sm-auto px-4">
                    <div class="pt-3 pb-2 mb-3 border-bottom text-center">
                        <h1 class="h2">MardSkills Event Platform</h1>
                    </div>
                    <form class="form-signin" method="POST" action="{{ route('admin.login') }}">
                        @csrf
                        <h1 class="h3 mb-3 font-weight-normal">Please sign in</h1>
                        <div class="form-group row">
                            <label for="inputEmail" class="sr-only">Email</label>
                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="email" required autofocus>
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            <label for="inputPassword" class="sr-only">Password</label>
                            <input type="password" id="inputPassword" name="password_hash" class="form-control" placeholder="password_hash">
                            @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password_hash') }}</strong>
                                    </span>
                            @endif
                            <button class="btn btn-lg btn-primary btn-block" id="login" type="submit">Sign in</button>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>

                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                            </div>
                        </div>
                    </form>
                </main>
    </div>
</div>
@endsection
