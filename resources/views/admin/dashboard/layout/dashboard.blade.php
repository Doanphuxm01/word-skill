<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>@yield('title', 'Admin')</title>

    <base href="{{asset('')}}">
    <!-- Bootstrap core CSS -->
    <link href="{{ asset('backend/css/custom.css') }}" rel="stylesheet">
    <link href="{{asset('backend/css/bootstrap.css')}}" rel="stylesheet">
    <link href="{{asset('backend/js/roomchajs.js')}}" rel="stylesheet">
    @yield('after-css')
</head>

<body>
    {{-- header --}}
    @include('admin.dashboard.includes.header')
    {{-- end header --}}
   {{-- page contet --}}
    <div class="container-fluid">
        <div class="row">
            {{-- nav --}}
            @include('admin.dashboard.includes.nav')
            {{-- end nav --}}
            @yield('content')
        </div>
    </div>
    @yield('after-script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
    <script
        src="https://code.jquery.com/jquery-3.4.1.js"
        integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU="
        crossorigin="anonymous"></script>
</body>
</html>
