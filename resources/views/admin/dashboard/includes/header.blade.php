<nav class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
    <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="{{ route('home') }}">Event Platform</a>
<span class="navbar-organizer w-100">{{ Auth::user()->name}}</span>
   <ul class="navbar-nav px-3">
       <li class="nav-item text-nowrap">
           <a class="nav-link" href="{{ route('admin.logout') }}"document.getElementById('logout-form').submit();">Logout</a>
        <form id="logout-form" action="{{ route('admin.logout') }}" method="POST" style="display: none;">
            @csrf
       </form> 
     </li> 
   </ul>
    {{-- <a class="navbar-brand col-sm-3 col-md-2 mr-0" href="{{ route('admin.logout') }}">Event Platform</a> --}}
</nav>
