@extends('admin.dashboard.layout.dashboard')
@section('content')

    <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
        @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
        <div class="border-bottom mb-3 pt-3 pb-2">
            @foreach ( $events as $key)
                
           
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center">
            <h1 class="h2">{{ $key->name}}</h1>
            </div>
            <span class="h6">{{ $key->date }}</span>
        </div>

        <div class="mb-3 pt-3 pb-2">
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center">
                <h2 class="h4">Create new session</h2>
            </div>
        </div>

        <form method="POST"  novalidate action="{{ route('sessions.add',$key->id) }}">
            @csrf
            <div class="row">
                <div class="col-12 col-lg-4 mb-3">
                    <label for="selectType">Type</label>
                    <select class="form-control" id="selectType" name="type">
                        <option value="talk" selected>Talk</option>
                        <option value="workshop">Workshop</option>
                    </select>
                    @if($errors->has('type'))
                          <span style="color:lightcoral; float: left;" class="erro_x1">{{$errors->first('type')}}</span>
                      @endif
                </div>
            </div>

            <div class="row">
                <div class="col-12 col-lg-4 mb-3">
                    <label for="inputTitle">Title</label>
                    <!-- adding the class is-invalid to the input, shows the invalid feedback below -->
                    <input type="text" class="form-control is-invalid" id="inputTitle" name="title" placeholder="" value="">
                    @if($errors->has('title'))
                          <span style="color:lightcoral; float: left;" class="erro_x1">{{$errors->first('title')}}</span>
                    @endif
                    <div class="invalid-feedback">
                        Title is required.
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12 col-lg-4 mb-3">
                    <label for="inputSpeaker">Speaker</label>
                    <input type="text" class="form-control" id="inputSpeaker" name="speaker" placeholder="" value="">
                    @if($errors->has('speaker'))
                          <span style="color:lightcoral; float: left;" class="erro_x1">{{$errors->first('speaker')}}</span>
                    @endif
                </div>
            </div>

            <div class="row">
                <div class="col-12 col-lg-4 mb-3">
                    <label for="selectRoom">Room</label>
                    <select class="form-control" id="selectRoom" name="room_id">
                        @foreach ($rooms as $key )
                        <option value="{{ $key->id}}">Room_{{ $key->name}}</option>
                        @endforeach
                    </select>
                    @if($errors->has('room_id'))
                          <span style="color:lightcoral; float: left;" class="erro_x1">{{$errors->first('room_id')}}</span>
                      @endif
                </div>
            </div>

            <div class="row">
                <div class="col-12 col-lg-4 mb-3">
                    <label for="inputCost">Cost</label>
                    <input type="number" class="form-control" id="inputCost" name="cost" placeholder="" value="0">
                    @if($errors->has('cost'))
                          <span style="color:lightcoral; float: left;" class="erro_x1">{{$errors->first('cost')}}</span>
                    @endif
                </div>
            </div>

            <div class="row">
                <div class="col-12 col-lg-6 mb-3">
                    <label for="inputStart">Start</label>
                    <input type="date"
                        class="form-control"
                        id="inputStart"
                        name="start"
                        placeholder="yyyy-mm-dd HH:MM"
                        value="">
                        @if($errors->has('start'))
                          <span style="color:lightcoral; float: left;" class="erro_x1">{{$errors->first('start')}}</span>
                        @endif
                </div>
                <div class="col-12 col-lg-6 mb-3">
                    <label for="inputEnd">End</label>
                    <input type="date"
                        class="form-control"
                        id="inputEnd"
                        name="end"
                        placeholder="yyyy-mm-dd HH:MM"
                        value="">
                        @if($errors->has('end'))
                        <span style="color:lightcoral; float: left;" class="erro_x1">{{$errors->first('end')}}</span>
                      @endif
                </div>
            </div>

            <div class="row">
                <div class="col-12 mb-3">
                    <label for="textareaDescription">Description</label>
                    <textarea class="form-control" id="textareaDescription" name="description" placeholder="" rows="5"></textarea>
                    @if($errors->has('description'))
                        <span style="color:lightcoral; float: left;" class="erro_x1">{{$errors->first('description')}}</span>
                    @endif
                </div>
            </div>

            <hr class="mb-4">
            <button class="btn btn-primary" type="submit">Save session</button>
            <a href="events/detail.html" class="btn btn-link">Cancel</a>
        </form>
        @endforeach
    </main>
@endsection