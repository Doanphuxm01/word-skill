@extends('admin.dashboard.layout.dashboard')
@section('content')
        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
            <div class="border-bottom mb-3 pt-3 pb-2">
                <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center">
                    <h1 class="h2">{insert event name}</h1>
                </div>
                <span class="h6">{insert event date}</span>
            </div>

            <div class="mb-3 pt-3 pb-2">
                <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center">
                    <h2 class="h4">Create new ticket</h2>
                </div>
            </div>

            <form method="post" class="needs-validation"  action="{{ route('tickets.store') }}">
                @csrf
                <div class="row">
                    <div class="col-12 col-lg-4 mb-3">
                        <label for="inputName">Name</label>
                        <!-- adding the class is-invalid to the input, shows the invalid feedback below -->
                        <input required type="text" class="form-control is-invalid" id="inputName" name="name" placeholder="" value="">
                        @if ($errors->has('name'))
                            <li style="color:brown">{{ $errors->first('name') }}</li>
                        @endif
                        <input type="hidden" class="form-control is-invalid" id="inputName" name="event_id" placeholder="" value="1">
                        <div class="invalid-feedback">
                            Name is required.
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 col-lg-4 mb-3">
                        <label for="inputCost">Cost</label>
                        <input type="number" class="form-control" id="inputCost" name="cost" placeholder="" value="0">
                        @if ($errors->has('cost'))
                            <li style="color:brown">{{ $errors->first('cost') }}</li>
                        @endif
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 col-lg-4 mb-3">
                        <label for="selectSpecialValidity">Special Validity</label>
                        <select class="form-control" id="selectSpecialValidity" name="special_validity">
                            <option value="" selected>None</option>
                            <option value="amount">Limited amount</option>
                            <option value="date">Purchaseable till date</option>
                        </select>
                        @if ($errors->has('special_validity'))
                        <li style="color:brown">{{ $errors->first('special_validity') }}</li>
                    @endif
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 col-lg-4 mb-3">
                        <label for="inputAmount">Maximum amount of tickets to be sold</label>
                        <input type="number" class="form-control" id="inputAmount" name="amount" placeholder="" value="">
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 col-lg-4 mb-3">
                        <label for="inputValidTill">Tickets can be sold until</label>
                        <input type="text"
                               class="form-control"
                               id="inputValidTill"
                               name="date"
                               placeholder="yyyy-mm-dd HH:MM"
                               value="">
                    </div>
                </div>
                <hr class="mb-4">
                <button  class="btn btn-primary" type="submit">Save ticket</button>
                {{-- onclick="location.href = document.referrer; return false;" --}}
                <button onclick="location.href = document.referrer; return false;" class="btn btn-link" type="submit">Cancel</button>
                {{-- <a  href="{{  }}" class="btn btn-link">Cancel</a>   --}}
            </form>

        </main>
        @endsection