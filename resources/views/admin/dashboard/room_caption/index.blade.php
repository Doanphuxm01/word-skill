@extends('admin.dashboard.layout.dashboard')
{{--@section('after-script')--}}
{{--    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>--}}
{{--@endsection--}}
@section('content')
    <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
{{--        <canvas id="line-chart"></canvas>--}}
        <canvas id="canvas" width="100%" height="100%"></canvas>
    </main>
@endsection
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>--}}
<script type="text/javascript">
    window.onload = function () {
        //
        var url = "{{url('home/charjs')}}";
        var capacity  = new Array ();
        var name  = new Array ();
        $(document).ready(function(){
            $.get(url, function(response){
                response.forEach(function(data){
                    capacity.push(data.capacity);
                    name.push(data.name);
                });
                var ctx = document.getElementById("canvas").getContext('2d');
                var myChart = new Chart(ctx, {
                    type: 'bar',
                    data: {
                        labels:name,
                        datasets: [
                            {
                            data: capacity,
                            label: 'Infosys Price',
                            backgroundColor: 'rgba(10, 128, 128, 0.8)',
                            borderColor: 'rgba(0, 128, 128, 0.7)',
                            borderWidth: 1
                            }
                        ]
                    },
                    options: {
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero:true
                                }
                            }]
                        }
                    }
                });
            });
        });
        //
    //     Chart.defaults.global.defaultFontColor = '#000000';
    //     Chart.defaults.global.defaultFontFamily = 'Arial';
    //     var lineChart = document.getElementById('line-chart');
    //     var myChart = new Chart(lineChart, {
    //         type: 'line',
    //         data: {
    //             labels: ["Jan", "Feb", "Mar", "Apr", "May", "June"],
    //             abels: ["Jan", "Feb", "Mar", "Apr", "May", "June"],
    //             datasets: [
    //                 {
    //                     label: 'PHP Activities',
    //                     data: [80, 30, 63, 20, 110, 3],
    //                     backgroundColor: 'rgba(0, 128, 128, 0.3)',
    //                     borderColor: 'rgba(0, 128, 128, 0.7)',
    //                     borderWidth: 1
    //                 },
    //                 {
    //                     label: 'Ruby Activities',
    //                     data: [18, 72, 10, 39, 19, 75],
    //                     backgroundColor: 'rgba(0, 128, 128, 0.7)',
    //                     borderColor: 'rgba(0, 128, 128, 1)',
    //                     borderWidth: 1
    //                 }
    //             ]
    //         },
    //         options: {
    //             scales: {
    //                 yAxes: [{
    //                     ticks: {
    //                         beginAtZero:true
    //                     }
    //                 }]
    //             },
    //         }
    //     });
    };
</script>
