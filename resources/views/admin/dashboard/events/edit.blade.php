
@extends('admin.dashboard.layout.dashboard')

@section('content')
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="border-bottom mb-3 pt-3 pb-2">
        <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center">
            <h1 class="h2">{{ $events->name }}</h1>
        </div>
    </div>

    <form method="post" class="needs-validation" novalidate action="{{ route('events.update',$events->id) }}">
        @method('PATCH')
        @csrf
        <div class="row">
            <div class="col-12 col-lg-4 mb-3">
                <label for="inputName">Name</label>
                <!-- adding the class is-invalid to the input, shows the invalid feedback below -->
                <input type="text" class="form-control is-invalid" id="inputName" name="name" placeholder="" value="{{ $events->name }}">
                @if ($errors->has('name'))
                            <li style="color:brown">{{ $errors->first('name') }}</li>
                @endif
                <div class="invalid-feedback">
                    Name is required.
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-12 col-lg-4 mb-3">
                <label for="inputSlug">Slug</label>
                <input type="text" class="form-control" name="slug" id="inputSlug" placeholder="" value="{{ $events->slug }}">
                @if ($errors->has('slug'))
                        <li style="color:brown">{{ $errors->first('slug') }}</li>
                @endif
            </div>
        </div>

        <div class="row">
            <div class="col-12 col-lg-4 mb-3">
                <label for="inputDate">Date</label>
                <input type="text"
                name="date"
                       class="form-control"
                       id="inputDate"
                       placeholder="yyyy-mm-dd"
                       value="{{ $events->date }}">
                        @if ($errors->has('date'))
                            <li style="color:brown">{{ $errors->first('date') }}</li>
                        @endif
            </div>
        </div>

        <hr class="mb-4">
        <button class="btn btn-primary" type="submit">Save</button>
        <a href="{{ route('events.index') }}" class="btn btn-link">Cancel</a>
    </form>

</main>
@endsection