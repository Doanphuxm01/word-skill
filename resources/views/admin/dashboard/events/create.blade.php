@extends('admin.dashboard.layout.dashboard')
@section('content')

        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
            {{-- @if ($errors->any())
                                                <div class="alert alert-danger">
                                                    <ul class="list-group mt-2">
                                                        @foreach ($errors->all() as $error)
                                                            <p>{{ $error }}</p>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            @endif --}}
            <div
                class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h1 class="h2">Manage Events</h1>
            </div>

            <div class="mb-3 pt-3 pb-2">
                <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center">
                    <h2 class="h4">Create new event</h2>
                </div>
            </div>

            <form  class="needs-validation" novalidate action="{{ route('events.store') }}" method="post">
                @csrf
                <div class="row">
                    <div class="col-12 col-lg-4 mb-3">
                        <label for="inputName">Name</label>
                        <!-- adding the class is-invalid to the input, shows the invalid feedback below -->
                        <input type="text" class="form-control is-invalid" id="inputName" name="name" placeholder="" value="">
                        @if ($errors->has('name'))
                            <li style="color:brown">{{ $errors->first('name') }}</li>
                        @endif
                        <input type="hidden" class="form-control is-invalid" id="inputName" name="organizer_id" placeholder="" value="1">
                        <div class="invalid-feedback">
                            Name is required.
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 col-lg-4 mb-3">
                        <label for="inputSlug">Slug</label>
                        <input type="text" class="form-control" id="inputSlug" name="slug" placeholder="" value="">
                        @if ($errors->has('slug'))
                        <li style="color:brown">{{ $errors->first('slug') }}</li>
                        @endif
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 col-lg-4 mb-3">
                        <label for="inputDate">Date</label>
                        <input type="date"
                               class="form-control"
                               id="inputDate"
                               name="date"
                               placeholder="yyyy-mm-dd"
                               value="">
                                @if ($errors->has('date'))
                                    <li style="color:brown">{{ $errors->first('date') }}</li>
                                @endif
                    </div>
                </div>

                <hr class="mb-4">
                <button class="btn btn-primary" type="submit">Save event</button>
                <a href="{{ route('events.index') }}">Cancel</a>
            </form>

        </main>
 @endsection
