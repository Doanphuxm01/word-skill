@extends('admin.dashboard.layout.dashboard')
@section('menu')
            @foreach ( $events as $key )
                <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
                    <span>{{ $key->name }}</span>
                </h6>
            @endforeach

            <ul class="nav flex-column">
                <li class="nav-item"><a class="nav-link active" href="events/detail.html">Overview</a></li>
            </ul>

            <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
                <span>Reports</span>
            </h6>
            <ul class="nav flex-column mb-2">
                <li class="nav-item"><a class="nav-link" href="{{ route('admin.rooms') }}">Room capacity</a></li>
            </ul>
        @endsection
@section('content')
    <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
        @if(session()->has('success'))
        <div class="alert alert-success">
            {{ session()->get('success') }}
        </div>
         @endif
        <div class="border-bottom mb-3 pt-3 pb-2 event-title">
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center">
                @foreach ($events as $key )
                <h1 class="h2"> {{ $key['name']}}</h1>
                <div class="btn-toolbar mb-2 mb-md-0">
                    <div class="btn-group mr-2">
                            @if(isset( $key->id))
                                <a href="{{ route('events.edit',$key->id) }}" class="btn btn-sm btn-outline-secondary">Edit event</a>
                            @endif
                    </div>
                </div>
            </div>
        <span class="h6">{{ $key['date']}}</span>
            @endforeach
        </div>

        <!-- Tickets -->
        <div id="tickets" class="mb-3 pt-3 pb-2">
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center">
                <h2 class="h4">Tickets</h2>
                <div class="btn-toolbar mb-2 mb-md-0">
                    <div class="btn-group mr-2">
                        <a href="{{ route('tickets.create') }}" class="btn btn-sm btn-outline-secondary">
                            Create new ticket
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <div class="row tickets">
            @foreach ($tickets as $key)
                <div class="col-md-4">
                    <div class="card mb-4 shadow-sm">
                        <div class="card-body">
                            <h5 class="card-title">{{ $key->name }}</h5>
                            <p class="card-text">{{ $key->cost }}</p>
                            <p class="card-text">&nbsp;</p>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>

        <!-- Sessions -->
        <div id="sessions" class="mb-3 pt-3 pb-2">
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center">
                <h2 class="h4">Sessions</h2>
                <div class="btn-toolbar mb-2 mb-md-0">
                    <div class="btn-group mr-2">
                        @foreach ($events as $key )
                    <a href="{{ route('sessions.create',$key->id)}}" class="btn btn-sm btn-outline-secondary">
                            Create new session
                        </a>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>

        <div class="table-responsive sessions">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Time</th>
                    <th>Type</th>
                    <th class="w-100">Title</th>
                    <th>Speaker</th>
                    <th>Channel</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ( $session as $key )
                    <tr>
                        <td class="text-nowrap">{{ $key->start }} - {{ $key->end }} </td>
                        <td> {{ $key->title }} </td>
                        <td><a href="sessions/edit.html"> {{ $key->title }} </a></td>
                        <td class="text-nowrap"> {{ $key->speaker }} </td>
                        <td class="text-nowrap"> {{ $key->room_id }} </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            {{ $session->links() }}
        </div>

        <!-- Channels -->
        <div id="channels" class="mb-3 pt-3 pb-2">
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center">
                <h2 class="h4">Channels</h2>
                <div class="btn-toolbar mb-2 mb-md-0">
                    <div class="btn-group mr-2">
                        {{-- {{ dd($events) }} --}}
                        @foreach ($events as $key )
                             <a href="{{ route('channels.create',$key->id) }}" class="btn btn-sm btn-outline-secondary">
                            Create new channel
                        </a>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>

        <div class="row channels">
            @foreach ($channels as $key )
            {{-- {{ dd($channels) }} --}}
            <div class="col-md-4">
                <div class="card mb-4 shadow-sm">
                    <div class="card-body">
                    <h5 class="card-title">{{ $key->name}}</h5>
                        <p class="card-text">3 sessions, 1 room</p>
                    </div>
                </div>
            </div>
            @endforeach
        </div>

        <!-- Rooms -->
        <div id="rooms" class="mb-3 pt-3 pb-2">
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center">
                <h2 class="h4">Rooms</h2>
                <div class="btn-toolbar mb-2 mb-md-0">
                    <div class="btn-group mr-2">
                        @foreach ( $events as $key )
                        <a href="{{ route('rooms.index',$key->id) }}" class="btn btn-sm btn-outline-secondary">
                            Create new room
                        </a>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>

        <div class="table-responsive rooms">
            <table class="table table-striped">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Capacity</th>
                </tr>
                </thead>
                <tbody>
                    @foreach ($rooms as $key )
                    <tr>
                        <td>{{ $key['name']}}</td>
                        <td>{{ $key['capacity']}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

    </main>
@endsection
